<?php

namespace app;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contratos;

/**
 * modelsContratosSearch represents the model behind the search form of `app\models\Contratos`.
 */
class modelsContratosSearch extends Contratos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_contrato', 'salario', 'opcion_jugador', 'clausula_antitraspaso', 'cod_jugador'], 'integer'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contratos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cod_contrato' => $this->cod_contrato,
            'salario' => $this->salario,
            'opcion_jugador' => $this->opcion_jugador,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'clausula_antitraspaso' => $this->clausula_antitraspaso,
            'cod_jugador' => $this->cod_jugador,
        ]);

        return $dataProvider;
    }
}
