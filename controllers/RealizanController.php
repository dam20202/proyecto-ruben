<?php

namespace app\controllers;

use Yii;
use app\models\Realizan;
use app\models\RealizanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * RealizanController implements the CRUD actions for Realizan model.
 */
class RealizanController extends Controller
{
    /**
     * {@inheritdoc}
     */
         
    public function actionEntrenos() {
$dataProvider = new ActiveDataProvider([
'query' => \app\models\Realizan::find()
->select('cod_realizan,nombre,
                        apellidos,
                        distancia_jugador,
                        calorias_jugador,
                        fecha')
->innerJoin('jugadores', 'realizan.cod_jugador = jugadores.cod_jugador')
->innerJoin('entrenamientos','realizan.cod_entrenamiento=entrenamientos.cod_entrenamiento'),
    'pagination'=>false,
]);
return $this->render("entrenos", [
"resultados" => $dataProvider,
]);
}
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Realizan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RealizanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Realizan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Realizan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Realizan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('entrenos');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Realizan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('../entrenos');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Realizan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['entrenos']);
    }

    /**
     * Finds the Realizan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Realizan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Realizan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
