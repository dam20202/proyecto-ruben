<?php

namespace app\controllers;

use Yii;
use app\models\Partidos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Jugadores;
use app\models\Juegan;
use yii\web\UploadedFile;

/**
 * PartidosController implements the CRUD actions for Partidos model.
 */
class PartidosController extends Controller {

    public function actionResultados() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Partidos::find()
                    ->select('partidos.cod_partido,partidos.nombre_rival,
        sum(juegan.puntos_jugador) nosotros,
        partidos.puntos_rivales,
        SUM(juegan.puntos_jugador)>partidos.puntos_rivales as resultado,
        mes,fecha,
        estadio')
                    ->innerJoin('juegan', 'juegan.cod_partido = partidos.cod_partido')
                    ->groupBy('juegan.cod_partido'),
            'pagination' => false,
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
        ]);
    }
     public function actionResultadosuser() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Partidos::find()
                    ->select('partidos.cod_partido,partidos.nombre_rival,
        sum(juegan.puntos_jugador) nosotros,
        partidos.puntos_rivales,
        SUM(juegan.puntos_jugador)>partidos.puntos_rivales as resultado,
        mes,fecha,
        estadio')
                    ->innerJoin('juegan', 'juegan.cod_partido = partidos.cod_partido')
                    ->groupBy('juegan.cod_partido'),
            'pagination' => false,
        ]);
        return $this->render("resultadosuser", [
                    "resultados" => $dataProvider,
        ]);
    }
     public function actionPartido($cod) {
      
        return $this->render("partido", [
                    "cod" => $cod,
        ]);
    }
     public function actionPartidouser($cod) {
      
        return $this->render("partidouser", [
                    "cod" => $cod,
        ]);
    }
    
    public function actionResultadospormes($mes=null) {
        if($mes==null){
            $mes = date('m');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Partidos::find()
                    ->select('partidos.cod_partido,partidos.nombre_rival,
        sum(juegan.puntos_jugador) nosotros,
        partidos.puntos_rivales,
        SUM(juegan.puntos_jugador)>partidos.puntos_rivales as resultado,
        mes,fecha,
        estadio')
                    ->innerJoin('juegan', 'juegan.cod_partido = partidos.cod_partido')
                    ->groupBy('juegan.cod_partido')
                    ->where("month(partidos.fecha)=$mes"),
            
            'pagination' => false,
        ]);
        return $this->render("resultadosmes", [
                    "mes"=>$mes,
                    "resultados" => $dataProvider,
        ]);
    }
     public function actionResultadospormesuser($mes=null) {
        if($mes==null){
            $mes = date('m');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Partidos::find()
                    ->select('partidos.cod_partido,partidos.nombre_rival,
        sum(juegan.puntos_jugador) nosotros,
        partidos.puntos_rivales,
        SUM(juegan.puntos_jugador)>partidos.puntos_rivales as resultado,
        mes,fecha,
        estadio')
                    ->innerJoin('juegan', 'juegan.cod_partido = partidos.cod_partido')
                    ->groupBy('juegan.cod_partido')
                    ->where("month(partidos.fecha)=$mes"),
            
            'pagination' => false,
        ]);
        return $this->render("resultadosmesuser", [
                    "mes"=>$mes,
                    "resultados" => $dataProvider,
        ]);
    }

    public function actionVistaindividual($cod) {
    
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Juegan::find()
                    ->select('
                    nombre,
                    cod_partido,
                    apellidos,
                    puntos_jugador,
                    rebotes_jugador,
                    asistencias_jugador,
                    robos,
                    minutos,
                    SUM(aciertos_jugador)/SUM(tiros_jugador)*100 AS TC,
                    SUM(t3_acertados)/SUM(t3_intentados)*100 T3,
                    SUM(minutos_jugador) minutos,
                    SUM(tl_acertados)/SUM(tl_intentados)*100 TL,
                    tapones')
                    ->innerJoin('jugadores', 'juegan.cod_jugador = jugadores.cod_jugador')
                    ->where('cod_partido='.$cod)
                    ->andWhere('puntos_jugador is not null')
                    ->groupBy('jugadores.cod_jugador'),
            'pagination' => false,
            'sort' => ['attributes' => ['nombre', 'apellidos', 'puntos_jugador', 'rebotes_jugador', 'asistencias_jugador','robos','minutos','TC', 'T3', 'TL','tapones']]
        ]);
        return $this->render("individuales", [
                    "resultados" => $dataProvider,
                     "cod"=>$cod
            
        ]);
    }
      public function actionVistaindividualuser($cod) {
    
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Juegan::find()
                    ->select('
                    nombre,
                    cod_partido,
                    apellidos,
                    puntos_jugador,
                    rebotes_jugador,
                    asistencias_jugador,
                    robos,
                    minutos,
                    SUM(aciertos_jugador)/SUM(tiros_jugador)*100 AS TC,
                    SUM(t3_acertados)/SUM(t3_intentados)*100 T3,
                    SUM(minutos_jugador) minutos,
                    SUM(tl_acertados)/SUM(tl_intentados)*100 TL,
                    tapones')
                    ->innerJoin('jugadores', 'juegan.cod_jugador = jugadores.cod_jugador')
                    ->where('cod_partido='.$cod)
                    ->andWhere('puntos_jugador is not null')
                    ->groupBy('jugadores.cod_jugador'),
            'pagination' => false,
            'sort' => ['attributes' => ['nombre', 'apellidos', 'puntos_jugador', 'rebotes_jugador', 'asistencias_jugador','robos','minutos','TC', 'T3', 'TL','tapones']]
        ]);
        return $this->render("individualesuser", [
                    "resultados" => $dataProvider,
                     "cod"=>$cod
            
        ]);
    }

    public function actionMaximos() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Partidos::find()
                    ->select('month(fecha)fechames,nombre_rival,
                            SUM(j.puntos_jugador) AS puntos,
                            SUM(j.aciertos_jugador)/SUM(j.tiros_jugador)*100 AS TC,
                            SUM(j.t3_acertados)/SUM(j.t3_intentados)*100 AS T3,
                            SUM(j.tl_acertados)/SUM(j.tl_intentados)*100 AS TL')
                    ->innerJoin('juegan j', 'partidos.cod_partido=j.cod_partido')
                    ->groupBy('fechames'),
            'sort' => ['attributes' => ['fechames', 'puntos', 'TC', 'T3', 'TL']]
        ]);

        $dataProvider6 = new ActiveDataProvider([
            'query' => \app\models\Partidos::find()
                    ->select('month(fecha)fechames,nombre_rival,
                            SUM(j.puntos_jugador)puntos,
                            SUM(j.aciertos_jugador)/SUM(j.tiros_jugador)*100 AS TC,
                            SUM(j.t3_acertados)/SUM(j.t3_intentados)*100 AS T3,
                            SUM(j.tl_acertados)/SUM(j.tl_intentados)*100 AS TL')
                    ->innerJoin('juegan j', 'partidos.cod_partido=j.cod_partido')
                    ->groupBy('nombre_rival'),
            'pagination' => false,
            'sort' => ['attributes' => ['nombre_rival', 'puntos', 'TC', 'T3', 'TL']]
        ]);

        return $this->render("globales", [
                    "resultados" => $dataProvider,
                    "resultados6" => $dataProvider6,
//                    "resultados9" => $dataProvider9,
        ]);
    }
 

    public function actionLideres() {
        $dataProvider2 = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find()
                    ->select('nombre,apellidos,
                            avg(puntos_jugador) puntos')
                    ->innerJoin('juegan j', 'jugadores.cod_jugador=j.cod_jugador')
                    ->where('jugadores.estado=1')
                    ->groupBy('jugadores.cod_jugador')
                    ->orderBy(['puntos' => SORT_DESC])
                    ->limit(1),
            'pagination' => false
        ]);
        $dataProvider3 = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find()
                    ->select('nombre,apellidos,
                            avg(rebotes_jugador) rebotes')
                    ->innerJoin('juegan j', 'jugadores.cod_jugador=j.cod_jugador')
                    ->where('jugadores.estado=1')
                    ->groupBy('jugadores.cod_jugador')
                    ->orderBy(['rebotes' => SORT_DESC])
                    ->limit(1),
            'pagination' => false
        ]);
        $dataProvider4 = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find()
                    ->select('nombre,apellidos,
                            avg(asistencias_jugador) asistencias')
                    ->innerJoin('juegan j', 'jugadores.cod_jugador=j.cod_jugador')
                    ->where('jugadores.estado=1')
                    ->groupBy('jugadores.cod_jugador')
                    ->orderBy(['asistencias' => SORT_DESC])
                    ->limit(1),
            'pagination' => false
        ]);
        $dataProvider5 = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find()
                    ->select('nombre,apellidos,
                            avg(robos) robos')
                    ->innerJoin('juegan j', 'jugadores.cod_jugador=j.cod_jugador')
                    ->where('jugadores.estado=1')
                    ->groupBy('jugadores.cod_jugador')
                    ->orderBy(['robos' => SORT_DESC])
                    ->limit(1),
            'pagination' => false
        ]);
        return $this->render("lideres", [
                    "resultados2" => $dataProvider2,
                    "resultados3" => $dataProvider3,
                    "resultados4" => $dataProvider4,
                    "resultados5" => $dataProvider5,
//                    "resultados9" => $dataProvider9,
        ]);
    }

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Partidos models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Partidos::find(),
        ]);



        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partidos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Partidos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        
        $model = new Partidos();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
                $model->video = UploadedFile::getInstance($model, 'video');
                if ($model->video = UploadedFile::getInstance($model, 'video')) {
                    if ($model->validate()) {
                        $model->video->saveAs('../web/videos/' . $model->cod_partido . '.' . $model->video->extension);
                    }
                }
            }
            return $this->redirect(array('juegan/actualizar', 'cod' => $model->cod_partido));
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Partidos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
                $model->video = UploadedFile::getInstance($model, 'video');
                if ($model->video = UploadedFile::getInstance($model, 'video')) {
                    if ($model->validate()) {
                        $model->video->saveAs('../web/videos/' . $model->cod_partido . '.' . $model->video->extension);
                    }
                }
            }
           return $this->redirect(array('juegan/actualizar', 'cod' => $model->cod_partido));
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Partidos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['resultados']);
    }

    /**
     * Finds the Partidos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partidos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Partidos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
