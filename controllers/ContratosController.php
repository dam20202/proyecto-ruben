<?php

namespace app\controllers;

use Yii;
use app\models\Contratos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContratosController implements the CRUD actions for Contratos model.
 */
class ContratosController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function actionContratos() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
                    ->select('
                            cod_contrato,
                            nombre,
                            apellidos,
                            año1,
                            año2,
                            año3,
                            clausula_antitraspaso,
                            opcion_jugador,
                            fecha_inicio,
                            fecha_fin')
                    ->innerJoin('jugadores j', 'contratos.cod_jugador=j.cod_jugador')
                    ->where('(estado=1 AND j.agente_libre=0)OR(agente_libre=0 AND estado IS NULL)'),
            'sort' => ['attributes' => ['nombre', 'salario', 'fecha_inicio', 'fecha_fin', 'año1', 'año2', 'año3', 'clausula_antitraspaso', 'opcion_jugador']]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
        ]);
    }

    public function actionRenovaciones() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
                    ->select('
                            cod_contrato,
                           j.cod_jugador,
                            nombre,
                            apellidos,
                            año1,
                            año2,
                            año3,
                            clausula_antitraspaso,
                            fecha_inicio,
                            fecha_fin')
                    ->innerJoin('jugadores j', 'contratos.cod_jugador=j.cod_jugador')
                    ->where('estado=1')
                    ->andWhere('año2 is null'),
            'sort' => ['attributes' => ['nombre', 'salario', 'fecha_inicio', 'fecha_fin']]
        ]);
        return $this->render("renovaciones", [
                    "resultados" => $dataProvider,
        ]);
    }

    public function actionMargen() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
                    ->select('
                            sum(año1) AS salario')->scalar()
//                          
        ]);
        $dataProvideraño2 = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
                    ->select('
                            sum(año2) AS salario')->scalar()
//                          
        ]);
        $dataProvideraño3 = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
                    ->select('
                            sum(año3) AS salario')->scalar()
//                          
        ]);
        $dataProvider2 = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
                    ->select('
                            (109140000-sum(año1)) AS "margens"')->scalar()
//                          
        ]);
        $dataProvider3 = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
                    ->select('
                            (132637000-sum(año1)) AS "margeni"')->scalar()
//                          
        ]);
        $dataProvider4 = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
//                  SELECT j.nombre,j.apellidos FROM contratos c INNER JOIN jugadores j ON c.cod_jugador = j.cod_jugador WHERE c.año1=(SELECT MAX(c.año1) FROM contratos c)
                    ->select('nombre')
                    ->innerJoin("jugadores", "jugadores.cod_jugador=contratos.cod_jugador")
                    ->where("año1=(select max(año1) from contratos)")
                    ->scalar()
//                          
        ]);
         $dataProvider7 = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
//                  SELECT j.nombre,j.apellidos FROM contratos c INNER JOIN jugadores j ON c.cod_jugador = j.cod_jugador WHERE c.año1=(SELECT MAX(c.año1) FROM contratos c)
                    ->select('apellidos')
                    ->innerJoin("jugadores", "jugadores.cod_jugador=contratos.cod_jugador")
                    ->where("año1=(select max(año1) from contratos)")
                    ->scalar()
//                          
        ]);
        $dataProvider5 = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
//                  SELECT j.nombre,j.apellidos FROM contratos c INNER JOIN jugadores j ON c.cod_jugador = j.cod_jugador WHERE c.año1=(SELECT MAX(c.año1) FROM contratos c)
                    ->select('apellidos')
                    ->innerJoin("jugadores", "jugadores.cod_jugador=contratos.cod_jugador")
                    ->where("año1=(select max(año1) from contratos)")
                    ->scalar()
//                          
        ]);
        

        $dataProvider6 = new ActiveDataProvider([
            'query' => \app\models\Contratos::find()
                    ->select('max(año1)')
                    ->scalar()
//                          
        ]);
        return $this->render("vistamargen", [
                    "margen" => $dataProvider,
                    "margen2" => $dataProvider2,
                    "margen3" => $dataProvider3,
                    "margen4" => $dataProvider4,
                    "margen5" => $dataProvider5,
                    "margen6" => $dataProvider6,
             "margen9" => $dataProvider7,
                    "margen7" => $dataProvideraño2,
                    "margen8" => $dataProvideraño3,
        ]);
    }

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contratos models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Contratos::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contratos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contratos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cod) {
        $model = new Contratos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('../contratos/contratos');
        }

        return $this->render('create', [
                    'model' => $model,
                    'cod' => $cod,
        ]);
    }

    public function actionCreate_1() {
        $model = new Contratos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['contratos']);
        }

        return $this->render('create_1', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Contratos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['contratos']);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Contratos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['contratos']);
    }

    /**
     * Finds the Contratos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contratos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Contratos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
