<?php

namespace app\controllers;

use Yii;
use app\models\Jugadores;
use app\models\JugadoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use yii\web\UploadedFile;

/**
 * JugadoresController implements the CRUD actions for jugadores model.
 */
class JugadoresController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function actionRoster() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find()
                    ->select('nombre,apellidos,puesto,numero,jugadores.imagen,count(partidos.cod_partido)partidosjugados,AVG(puntos_jugador) puntos,avg(asistencias_jugador)asistencias,avg(rebotes_jugador)rebotes,altura,envergadura,TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE())fecha')
                    ->innerJoin('juegan','juegan.cod_jugador=jugadores.cod_jugador')
            ->innerJoin('partidos','juegan.cod_partido=partidos.cod_partido')
                    ->where('estado=1')
                    ->andWhere('agente_libre=0')
                ->andWhere('juegan.puntos_jugador is not null')
                ->groupBy('juegan.cod_jugador'),
            'pagination' => false,
        ]);
        return $this->render("roster", [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionEstadisticas() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find()
                    ->select('jugadores.cod_jugador,
                        jugadores.nombre,
                        jugadores.apellidos,
                        jugadores.puesto,
                        jugadores.altura,
                        jugadores.envergadura,
                        fecha_nacimiento,
                        tapones,
                        robos,
                        AVG(j.puntos_jugador) puntos,
                        AVG(j.rebotes_jugador) rebotes,
                        AVG(j.asistencias_jugador) asistencias,
                        SUM(j.aciertos_jugador)/SUM(j.tiros_jugador)*100 AS TC,
                        SUM(j.t3_acertados)/SUM(j.t3_intentados)*100 T3,
                        SUM(j.minutos_jugador) minutos,
                        SUM(j.tl_acertados)/SUM(j.tl_intentados)*100 TL')
                    ->leftJoin('juegan j', 'j.cod_jugador = jugadores.cod_jugador')
                    ->where('(estado=1 AND agente_libre=0)OR(agente_libre=0 AND estado IS NULL)OR(estado=1 AND agente_libre IS NULL)')
                    ->groupBy('jugadores.cod_jugador'),
            'sort' => ['attributes' => ['nombre', 'fecha_nacimiento', 'apellidos', 'puesto', 'puntos', 'rebotes', 'asistencias', 'altura', 'envergadura', 'minutos', 'TC', 'T3', 'TL', 'robos']]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
        ]);
    }

    public function actionEstadisticasuser() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find()
                    ->select('jugadores.cod_jugador,
                        jugadores.nombre,
                        jugadores.apellidos,
                        jugadores.puesto,
                        jugadores.altura,
                        jugadores.envergadura,
                        fecha_nacimiento,
                        tapones,
                        robos,
                        AVG(j.puntos_jugador) puntos,
                        AVG(j.rebotes_jugador) rebotes,
                        AVG(j.asistencias_jugador) asistencias,
                        SUM(j.aciertos_jugador)/SUM(j.tiros_jugador)*100 AS TC,
                        SUM(j.t3_acertados)/SUM(j.t3_intentados)*100 T3,
                        SUM(j.minutos_jugador) minutos,
                        SUM(j.tl_acertados)/SUM(j.tl_intentados)*100 TL')
                    ->leftJoin('juegan j', 'j.cod_jugador = jugadores.cod_jugador')
                    ->where('(estado=1 AND agente_libre=0)OR(agente_libre=0 AND estado IS NULL)OR(estado=1 AND agente_libre IS NULL)')
                    ->groupBy('jugadores.cod_jugador'),
            'sort' => ['attributes' => ['nombre', 'fecha_nacimiento', 'apellidos', 'puesto', 'puntos', 'rebotes', 'asistencias', 'altura', 'envergadura', 'minutos', 'TC', 'T3', 'TL', 'robos']]
        ]);
        return $this->render("estadisticas", [
                    "resultados" => $dataProvider,
        ]);
    }

    public function actionAgentes() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find()
                    ->select('*')
                    ->where('agente_libre=1'),
            'pagination' => false,
// 'sort' => ['attributes' => ['nombre', 'apellidos', 'puntos', 'rebotes', 'asistencias', 'altura', 'envergadura', 'minutos', 'TC', 'T3', 'TL']]
        ]);
        return $this->render("agenteslibres", [
                    "resultados" => $dataProvider,
        ]);
    }

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all jugadores models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single jugadores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new jugadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate() {
//        $model = new jugadores();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->cod_jugador]);
//        }
//
//        return $this->render('create', [
//                    'model' => $model,
//        ]);
//    }

    public function actionCreate() {

        $model = new Jugadores();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
                $model->eventImage = UploadedFile::getInstance($model, 'eventImage');
                if ($model->eventImage = UploadedFile::getInstance($model, 'eventImage')) {
                    if ($model->validate()) {
                        $model->eventImage->saveAs('../web/img/' . $model->nombre . '.' . $model->eventImage->extension);
                    }
                }
            }
            return $this->redirect('estadisticas');
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isPost) {
                if ($model->eventImage = UploadedFile::getInstance($model, 'eventImage')) {

                    if ($model->validate()) {
                        $model->eventImage->saveAs('../web/img/' . $model->nombre . '.' . $model->eventImage->extension);
                    }
                }
            }
            return $this->redirect('../estadisticas');
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    public function actionUpload($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->eventImage = UploadedFile::getInstance($model, 'eventImage');
            if ($model->upload()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('upload', ['model' => $model]);
    }

//para crear un agente y ponerle agentre true automatico
    public function actionCreateagente() {
        $model = new \app\models\Jugadores();
        $model->agente_libre = 1;

        $model->save();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('agentes');
        }

        return $this->render('createagente', [
                    'model' => $model,
        ]);
    }

    public function actionCrearagente($id) {
        $model = $this->findModel($id);

        $model->agente_libre = 0;
        $model->estado = 1;

        $model->save(false);




        return $this->redirect(array('contratos/create', 'cod' => $model->cod_jugador));
    }

    /**
     * Updates an existing jugadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    /**
     * Deletes an existing jugadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//hay q arreglar las rules, algo no deja guardar
    public function actionDelete($id) {
        $model = $this->findModel($id);


        if ($model->agente_libre === 1) {
            $model->delete();
            return $this->redirect(['agentes']);
        } else {
            $model->estado = 2;
            $model->save(false);
            return $this->redirect(['jugadores/estadisticas']);
        }
    }

//   public function actionDeleteagente($id) {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['agentes']);
//    }

    /**
     * Finds the jugadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return jugadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = jugadores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
