<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RealizanEntrenosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="realizan-entrenos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cod_realizan') ?>

    <?= $form->field($model, 'cod_jugador') ?>

    <?= $form->field($model, 'cod_entrenamiento') ?>

    <?= $form->field($model, 'distancia_jugador') ?>

    <?= $form->field($model, 'calorias_jugador') ?>

    <?php // echo $form->field($model, 'fecha') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
