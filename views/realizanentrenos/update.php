<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RealizanEntrenos */

$this->title = 'Update Realizan Entrenos: ' . $model->cod_realizan;
$this->params['breadcrumbs'][] = ['label' => 'Realizan Entrenos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_realizan, 'url' => ['view', 'id' => $model->cod_realizan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="realizan-entrenos-update">

    <h1></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
