
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>Kevin Love JUGARÁ CON EL TEAM USA EN LOS JUEGOS OLÍMPICOS</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>

                    <img src="../../web/img/imagen20_1.jpg" alt="" style="
    width: 60%;
"/>
                </header>

                <p>
                    El delantero de los Cleveland Cavaliers , Kevin Love, se dirige a Japón.


                </p>
                <br>

                <p>           
                    Como informó por primera vez Adrian Wojnarowski de ESPN, Love jugará para el equipo de EE. UU. En los próximos Juegos Olímpicos de verano en Tokio. Love se une a personas como Kevin Durant, James Harden y Devin Booker como miembros confirmados del equipo olímpico.

                </p>  
                <br>
                <br>
                <div id="seppQf">
                    <div class="twitter-tweet twitter-tweet-rendered" style="display: flex; max-width: 550px; width: 100%; margin-top: 10px; margin-bottom: 10px;"><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="" style="position: static; visibility: visible; width: 550px; height: 249px; display: block; flex-grow: 1;" title="Tweet de Twitter" src="https://platform.twitter.com/embed/Tweet.html?dnt=false&amp;embedId=twitter-widget-0&amp;features=eyJ0ZndfZXhwZXJpbWVudHNfY29va2llX2V4cGlyYXRpb24iOnsiYnVja2V0IjoxMjA5NjAwLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X2hvcml6b25fdHdlZXRfZW1iZWRfOTU1NSI6eyJidWNrZXQiOiJodGUiLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X3R3ZWV0X2VtYmVkX2NsaWNrYWJpbGl0eV8xMjEwMiI6eyJidWNrZXQiOiJjb250cm9sIiwidmVyc2lvbiI6bnVsbH19&amp;frame=false&amp;hideCard=false&amp;hideThread=false&amp;id=1407367742081089538&amp;lang=es&amp;origin=https%3A%2F%2Fwww.fearthesword.com%2F2021%2F6%2F22%2F22542996%2Fkevin-love-to-play-for-team-usa-at-upcoming-olympics&amp;sessionId=2be8dd72a5b7ed3c84a8326d921d140dfddc9c5d&amp;siteScreenName=FearTheSword&amp;theme=light&amp;widgetsVersion=82e1070%3A1619632193066&amp;width=550px" data-tweet-id="1407367742081089538"></iframe></div>
                    <script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
                   <br>
                <p>
                    Si Love gana una medalla de oro, será la tercera vez que un Cavalier activo ganará una medalla de oro. No ha jugado para el equipo de EE. UU. Desde 2012, cuando formó parte de un equipo del equipo de EE. UU. Que quedó invicto en los Juegos Olímpicos de Londres y ganó una medalla de oro. También formó parte del equipo de 2010 que ganó el oro en el Campeonato Mundial FIBA.

                </p>
                <br>
                <br>
                <p>
                    En 2014, Love no participó en el Campeonato Mundial debido a las conversaciones comerciales en curso que finalmente lo llevaron a ser cambiado a los Cavaliers. Las lesiones y otros problemas lo han mantenido alejado del equipo de EE. UU. Desde entonces. En el escenario internacional, le gusta operar como un espaciador de piso de los creadores principales del equipo y probablemente prosperará en el papel.

                </p>
                <br>
                <br>
                <p>
                    También es una oportunidad para que Love a) obtenga algo de tiempo de juego después de varios años de inconsistencia yb) tal vez aumente su valor en el mercado comercial. Los Juegos Olímpicos comienzan el 23 de julio y se extenderán hasta el 8 de agosto.

                </p>

              
            </article>

    </div>


</body>



</html>