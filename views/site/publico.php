
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>SB Nation Reacts: Impacto de multitudes, entretenidas series y más</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>
                    
                    <img src="../../web/img/imagen6_1.jpg" alt="" style="
    width: 50%;
"/>
                </header>

                <p>         
Bienvenido a SB Nation Reacts , una encuesta de fanáticos de la NBA. Cada semana, enviamos preguntas a los fanáticos y fanáticos de los Cleveland Cavaliers más conectados en todo el país. Regístrese aquí para unirse a Reacts.

                </p>
                <br>

                <p>           
Con la primera ronda de los playoffs en marcha, la diferencia más notable en los juegos ha sido la incorporación de una multitud casi completa. Según la última encuesta de SB Nation Reacts, más del 50 por ciento de los fanáticos de todo el país dijeron que la inclusión de fanáticos ha mejorado significativamente la experiencia de visualización, solo el 16 por ciento dijo que no ha mejorado la experiencia en absoluto.

                </p>  
                         <br>
                                  <br>
                <p>
Con el resto de fanáticos de todo el país mirando en casa, no hay escasez de grandes series de la ronda de apertura para disfrutar. El 30 por ciento de los fanáticos de todo el país cree que la serie Phoenix Suns vs. Los Angeles Lakers es la mejor del grupo. La serie de los New York Knicks contra los Atlanta Hawks es la siguiente opción más popular, con el 21 por ciento de los votos.

                </p>
                         <br>
                         <img srcset="https://cdn.vox-cdn.com/thumbor/nYkpXyJcAIzUKLW8NuPfaVN64BY=/0x0:1080x1350/320x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg 320w, https://cdn.vox-cdn.com/thumbor/oqpUTO_CrX9Znpwvlm8kzyscIP8=/0x0:1080x1350/520x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg 520w, https://cdn.vox-cdn.com/thumbor/q69Hskl65UPv8Kx4QYMznXXS9M8=/0x0:1080x1350/720x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg 720w, https://cdn.vox-cdn.com/thumbor/ixulDZZYYmJVbpWRLffyuvrtaJ8=/0x0:1080x1350/920x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg 920w, https://cdn.vox-cdn.com/thumbor/MGSGNc8WiPkfPY3d0khFJk5Gnwo=/0x0:1080x1350/1120x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg 1120w, https://cdn.vox-cdn.com/thumbor/zfNwyGYrB8np55O5sJeMHpqCIOc=/0x0:1080x1350/1320x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg 1320w, https://cdn.vox-cdn.com/thumbor/YP_zdcJ49akeC_0QyiRd-HxvUQs=/0x0:1080x1350/1520x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg 1520w, https://cdn.vox-cdn.com/thumbor/u7FBZQSiXBySj43VjcGQdA4acBY=/0x0:1080x1350/1720x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg 1720w, https://cdn.vox-cdn.com/thumbor/BoMqhQzGIDV24NNTn1WRFq-CloM=/0x0:1080x1350/1920x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg 1920w" sizes="(min-width: 1221px) 846px, (min-width: 880px) calc(100vw - 334px), 100vw" alt="" data-upload-width="1080" src="https://cdn.vox-cdn.com/thumbor/F8KmcYa_LLvTeyhTFsxIxFkioeM=/0x0:1080x1350/1200x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22541803/_National_2_52621.jpg">
                                  <br>
                <p>
De manera similar, el 26 por ciento de los fanáticos cree que los Suns muestran mejor las características de un equipo exitoso. Es curioso que haya una conexión directa con los Cavs allí con el ex delantero de los Cavs James Jones como gerente general. En realidad, hay muchos ex-Cavs esparcidos por todo el playoff, desde LeBron James hasta Cam Payne.

                </p>
                         <br>
                                  <br>
                <p>
Para votar en las encuestas de Reacts y que su voz sea escuchada cada semana, regístrese aquí .

                </p>
               

            </article>

    </div>


</body>



</html>