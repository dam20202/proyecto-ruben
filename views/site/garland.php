
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>Darius Garland se una al Team USA

</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>

                    <img id="noticia" src="../../web/img/garlandnoticia.jpg" alt=""/>
                </header>

                <p>
Durante su disponibilidad en los medios de comunicación después de la NBA Draft Lottery, el gerente general de los Cleveland Cavaliers compartió con los medios que el armador Darius Garland fue invitado a unirse a la lista del Team USA Select Team.

                </p>
                <br>

                <p>   
Garland se unirá al equipo del año pasado, que cuenta con su compañero Cavalier Jarrett Allen, y jugará contra su compañero Kevin Love y el resto de la lista de doce hombres del equipo de EE. UU. A finales de este verano en Las Vegas. Esta es una oportunidad increíble para que Garland continúe creciendo y desarrollándose mientras juega con los mejores del mundo.

                </p>  
                <br>
                <br>
                <p>
Su estrella está en ascenso y Garland es el futuro de los Cavs. Ser invitado a unirse al Team USA Select Team es indicativo de eso.

                </p>
              

           

            </article>

    </div>


</body>



</html>