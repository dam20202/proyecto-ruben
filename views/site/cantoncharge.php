
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>El afiliado de los Cavs G-League se muda de Canton a Cleveland</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>
                    
                    <img src="../../web/img/chargegrande.jpg" alt="" style="
    width: 50%;
"/>
                </header>

                <p>Los Canton Charge ya no existen.            
                    En cambio, pasarán por el Cleveland Charge la próxima temporada, ya que la filial de Cleveland Cavaliers G-League comenzará a jugar en el centro de la ciudad la próxima temporada en el Wolstein Center de la Universidad Estatal de Cleveland. Recientemente se tomó la decisión de no extender su contrato de arrendamiento vencido en el Canton Memorial Civic Center, poniendo fin a una estadía de 10 años y nueve temporadas en el Civic Center, que sirvió como la primera sede del equipo después de que los Cavaliers adquirieran la franquicia. reubicado de Nuevo México y rebautizado como Canton Charge en 2011.
                </p>
                <br>

                <p>           
                    “Queremos agradecer al alcalde Bernabei, la ciudad de Canton y nuestros fanáticos y socios de Charge por una década de emoción en el Civic Center”, dijo el director ejecutivo de Rock Entertainment Group, Len Komoroski, en un comunicado. “Cuando adquirimos la franquicia y la trasladamos aquí al noreste de Ohio, nuestro objetivo era crear la mejor operación y experiencia en la G-League, tanto dentro como fuera de la cancha. Hemos tenido éxito trabajando hacia ambos objetivos y ese sigue siendo nuestro compromiso y enfoque ".               
                </p>  
                         <br>
                                  <br>
                <p>
                    The Charge jugó una campaña abreviada 2020-21 en Orlando, como parte de la G League Bubble. El último juego en Canton fue hace más de un año. El Charge se fue de 5-10 durante la temporada abreviada.          
                </p>
                         <br>
                                  <br>
                <p>
                    “Estoy, por supuesto, decepcionado de que el Charge ya no juegue en Canton. Tuvimos conversaciones abiertas y profesionales con la administración de Charge sobre cómo extender su contrato de arrendamiento y quedarse ”, declaró el alcalde de Cantón, Thomas Bernabei. “Sin embargo, su movimiento es estimulado por el deseo de estar mucho más cerca del centro de operaciones central de los Cavaliers. Trabajamos duro para mantenerlos aquí, pero aprecio su franqueza en nuestras conversaciones, respeto su decisión y sinceramente les deseo lo mejor. También apreciamos su compromiso continuo y la expansión de la programación y el apoyo para los jóvenes y la comunidad aquí. Les agradecemos por una carrera emocionante en el Civic Center y seguiremos siendo fanáticos del Charge y los Cavs ”.               
                </p>
                         <br>
                                  <br>
                <p>
                    El traslado al Wolstein Center de la Universidad Estatal de Cleveland (CSU) sigue una tendencia en la G League, donde los equipos continúan operando mucho más cerca de las sedes y los centros de entrenamiento de la franquicia de la NBA. Ejemplos recientes de esto están presentes en Washington y Detroit, y más de la mitad de los equipos de la G League ahora se encuentran a menos de 30 millas de los estadios de juego e instalaciones de entrenamiento y desarrollo de su club matriz de la NBA.                
                </p>
                         <br>
                                  <br>
                <p>
                    El papel y la importancia de la G League dentro del panorama de desarrollo de la NBA continúa expandiéndose, ya que cerca del 60% de los jugadores que ahora están en la NBA han jugado en la G League. Jugar en el centro de Cleveland logra un mayor nivel de integración, recursos y flexibilidad operativa para los jugadores de Charge, el personal y los miembros de la oficina principal según la ubicación se encuentra a varias cuadras de Rocket Mortgage FieldHouse y a solo ocho millas de Cleveland Clinic Courts, el entrenamiento y el jugador centro de desarrollo de los Cavaliers. The Charge ahora podrá ejercer tanto en Rocket Mortgage FieldHouse como en Cleveland Clinic Courts, así como utilizar las instalaciones extendidas y los beneficios de tratamiento presentes en ambos lugares.                
                </p>
                         <br>
                                  <br>
                <p>
                    “Estamos muy agradecidos por las temporadas que jugamos en el Civic Center en Canton. Disfrutamos de momentos realmente especiales allí con nuestros jugadores, personal y fanáticos por igual ”, agregó el gerente general de Charge, Brendon Yu. “Al mirar hacia el futuro, estamos muy contentos de poder continuar con el crecimiento de Charge en el Wolstein Center. La proximidad a Cleveland Clinic Courts y Rocket Mortgage Fieldhouse crea una sinergia mejorada entre los dos equipos y fomentará un desarrollo de personal aún mayor que será muy impactante para nosotros ".                
                </p>
                <p>
                    La transición para jugar en CSU también inicia una nueva era de baloncesto Charge en el centro de Cleveland y una que estará acompañada por el desarrollo de una visión a largo plazo y una asociación con CSU que también puede alinearse con la evolución de sus planes futuros de sedes.             
                </p>
                         <br>
                                  <br>
                <p>
                    The Charge también anunció un compromiso para expandir los deportes juveniles, la programación comunitaria y el apoyo en Canton, incluidos los programas de campamento y clínica de los Cavs Academy y Jr. Cavs y los juegos Cavs Elite para brindar múltiples oportunidades nuevas a la comunidad de Canton. Este compromiso ampliado también aumentará la cantidad de oportunidades gratuitas para que participen los jóvenes locales desatendidos o desfavorecidos. Las mayores oportunidades también serán parte de un compromiso mejorado para abordar la escasez de alimentos en la región a través de la donación de ganancias de eventos selectos de campamentos o torneos que la organización de los Cavs igualará para generar un mínimo de 20,000 comidas adicionales para los esfuerzos locales, incluido el Akron. -Canton Foodbank y Stark Hunger Task Force.
                </p>

              
            </article>

    </div>


</body>



</html>