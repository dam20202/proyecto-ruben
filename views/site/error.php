<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
 <div class="kode-content">
	<div class="container">
		<div class="error-404">
			<h2>Whooos!!!</h2>
			<div class="page-404">
				<p>404</p>
				<span><?= nl2br(Html::encode($message)) ?></span>
			</div>
			<p>We could not found the page you are looking for. Please try another page and verify the URL you have entered.</p>
			<a href="http://www.kodeforest.net/books/library/" class="go-back">Go back to home page</a>
		</div>   
	</div>   
</div>
    </div>