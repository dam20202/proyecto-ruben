
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>Reseña de la temporada 2020-21 de Larry Nance Jr.</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>

                    <img src="../../web/img/imagenlarry.jpg" alt="" style="
    width: 50%;
"/>
                </header>


                <p>
                    Como muchos jugadores en la temporada de la NBA 2020-21 teñida de COVID, Larry Nance Jr. luchó contra enfermedades y lesiones jugando en solo 35 juegos. Lidiando con una fractura de pulgar y una misteriosa enfermedad que le hizo perder 20 libras en solo una semana, se podría argumentar (todavía) que Nance solidificó su papel como el principal defensor de Cleveland este año. El grandote de 28 años consiguió 1,7 robos por partido, el mejor de su carrera, y al mismo tiempo consiguió 222.000 dólares para las empresas locales, bastante correcto, debo decir.



                </p>
                <h2>El Defensor del Núcleo</h2>
                <br>

                <p>           
                    En un equipo de los Cavs que carece desesperadamente de ritmo y flujo ofensivo, Nance repetidamente le dio a su equipo un tiro a la defensiva ... creando posesiones adicionales con su enfoque fluido. Su carterista fue tan supremo de hecho, podría haberlo llevado tercero en la liga si hubiera alcanzado el mínimo estadístico de jugar el 70% de los juegos de su equipo. Esa estadística, entre otras, ayudó a Nance a obtener una calificación defensiva de 107esta temporada. Según Cleaning the Glass, los equipos anotaron -9.3 puntos por cada 100 posesiones con Nance en la cancha. Eso es lo suficientemente bueno como para incluir a los grandes de Cleveland en el percentil 97 entre todos los jugadores de la liga. La misma herramienta de investigación etiqueta a Nance con un porcentaje de robo de 2.5, ubicándolo en el percentil 99 o 10 en la liga detrás de Victor Oladipo. ¿Qué porcentaje de las posesiones de los oponentes terminaron en pérdidas de balón con Nance en la cancha? Eso se puede explicar mejor dentro de la estadística TOV%, también una medida sólida para Nance, que ocupa el décimo lugar en la liga con 3.3%.


                </p>  
                <br>
                <h2>La bola larga</h2>
                <br>


                <p>
                    Nance vio mejoras en su juego de perímetro este año, disparando un 36% de tres, el más alto de su carrera. Promedió 3.3 intentos por juego desde lo profundo, también un récord personal. Además de agregar el balón largo a su bolsa, Nance experimentó algunas leves caídas en su porcentaje general de tiros de campo (47%) y puntos por número de juego (9.3). Podríamos culpar al pulgar aquí, o incluso a su nuevo amor por las tres bolas, pero aquí hay un vistazo a su tabla de tiros 2020-21:


                </p>
                <img srcset="https://cdn.vox-cdn.com/thumbor/LWHfrlKysmimRx2Qxkc5dgMMRPY=/0x0:1165x1112/320x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png 320w, https://cdn.vox-cdn.com/thumbor/aGSYjpSkFwmGzviLQfrb8UIh9ic=/0x0:1165x1112/520x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png 520w, https://cdn.vox-cdn.com/thumbor/Fw3M0GVsWpltH2Ah9fhBgjCByu4=/0x0:1165x1112/720x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png 720w, https://cdn.vox-cdn.com/thumbor/cZhmG4f6lm2gHVreoQYETarvDgA=/0x0:1165x1112/920x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png 920w, https://cdn.vox-cdn.com/thumbor/gFW8VKOnNzTM1z8Kt3GFEVWYWTE=/0x0:1165x1112/1120x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png 1120w, https://cdn.vox-cdn.com/thumbor/Ll3iNpfWClwGkN5gWUnSDVsx0Sc=/0x0:1165x1112/1320x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png 1320w, https://cdn.vox-cdn.com/thumbor/skrNijTfDqmzAxus__PCKtvrIZE=/0x0:1165x1112/1520x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png 1520w, https://cdn.vox-cdn.com/thumbor/-heFy79uTowpm_sJZIP8BUnfKHY=/0x0:1165x1112/1720x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png 1720w, https://cdn.vox-cdn.com/thumbor/cNTF8bkxmXmavn7lKY8pDZrxKic=/0x0:1165x1112/1920x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png 1920w" sizes="(min-width: 1221px) 846px, (min-width: 880px) calc(100vw - 334px), 100vw" alt="" data-upload-width="1165" src="https://cdn.vox-cdn.com/thumbor/Yfy3xuWs5b5z85MMsxps5ts-HJ0=/0x0:1165x1112/1200x0/filters:focal(0x0:1165x1112):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22701184/Nance_ShotChart.png">
                <br>
                <h2>El juego "No te puedes perder"</h2>
                <br>
                <p>

                    Nance tuvo quizás su mejor juego de la temporada el 7 de enero en Memphis, con un perfecto 7-7 desde el campo y 4-4 desde tres. Esa misma noche, Nance se escapó con 3 robos y los Cavs consiguieron una impresionante victoria como visitantes por 94-90. Fue de lejos su mejor juego. Estuvo EN TODAS PARTES, compruébalo:


                </p>
                <iframe width="741" height="417" src="https://www.youtube.com/embed/iY5aU5Rh-_g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <br>
                <h2>El héroe de la ciudad natal:</h2>
                <br>
                <p>
                    Me enamoré de los Cleveland Cavaliers en 1989 cuando vi a 'mr-tube-socked-larry-nance' despegando en el Richfield Coliseum. Hasta donde yo sé, ese hombre encantador nunca se fue de Ohio; de hecho, dio un paso más y crió a Nance Jr. en Richfield también.


                </p>
                <br>
                <p>
                    Si bien todos anticipamos la temporada 2020-21 de la NBA un tanto desesperada y encerrados en nuestras casas, fue Larry Nance Jr. quien nos hizo sonreír y emocionar nuestros corazones. Nos prometió que usaría nuestras camisas mientras subastaba las suyas ... entregó por una suma de $ 222,000. Sus esfuerzos le valieron una nominación muy merecida para el NBA Cares Community Assist Award , el primer premio B / R Kicks Community Award y el respeto infinito de su comunidad local.



                </p>   
                <div class="twitter-tweet twitter-tweet-rendered" style="display: flex; max-width: 550px; width: 100%; margin-top: 10px; margin-bottom: 10px;"><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true" class="" style="position: static; visibility: visible; width: 550px; height: 600px; display: block; flex-grow: 1;" title="Tweet de Twitter" src="https://platform.twitter.com/embed/Tweet.html?dnt=false&amp;embedId=twitter-widget-0&amp;features=eyJ0ZndfZXhwZXJpbWVudHNfY29va2llX2V4cGlyYXRpb24iOnsiYnVja2V0IjoxMjA5NjAwLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X2hvcml6b25fdHdlZXRfZW1iZWRfOTU1NSI6eyJidWNrZXQiOiJodGUiLCJ2ZXJzaW9uIjpudWxsfSwidGZ3X3R3ZWV0X2VtYmVkX2NsaWNrYWJpbGl0eV8xMjEwMiI6eyJidWNrZXQiOiJjb250cm9sIiwidmVyc2lvbiI6bnVsbH19&amp;frame=false&amp;hideCard=false&amp;hideThread=false&amp;id=1408531570210881539&amp;lang=en&amp;origin=https%3A%2F%2Fwww.fearthesword.com%2F2021%2F7%2F7%2F22564973%2Flarry-nance-jr-2020-21-season-review&amp;sessionId=2657d5f92c64a91b867fa7da1fe308005cf846d3&amp;siteScreenName=FearTheSword&amp;theme=light&amp;widgetsVersion=82e1070%3A1619632193066&amp;width=550px" data-tweet-id="1408531570210881539"></iframe></div>

                <h2>El futuro</h2>
                <br>
                <p>
                    Nance firmó una extensión a escala de novato con los Cavs hasta 2023, y Cleveland haría bien en mantenerlo en su lista. Si puede seguir mejorando sus lanzamientos y su fuerza en la defensa, será una ventaja para cualquier equipo que busque un jugador grande con la capacidad de anotar desde cualquier lugar de la cancha.




                </p>  


            </article>

    </div>


</body>



</html>