
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>NBA Reacts: ¿Quién merece ganar el premio al jugador más mejorado?</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>
                    
                    <img src="../../web/img/imagen5_1.jpg" alt="" style="
    width: 50%;
"/>
                </header>

                <p>Bienvenido a SB Nation Reacts , una encuesta de fanáticos de la NBA. Cada semana, enviamos preguntas a los fanáticos de los Cleveland Cavaliers más conectados y a los fanáticos de todo el país. Regístrese aquí para unirse a Reacts.


                </p>
                <br>

                <p>           
A medida que se acercan los playoffs, la mayor sorpresa de la temporada probablemente sea el éxito de los New York Knicks . Por primera vez desde 2013, los Knicks llegaron a la postemporada y lo hicieron sin una verdadera superestrella al frente del equipo.

                </p>  
                         <br>
                                  <br>
                <p>
En lugar de una superestrella, el equipo está dirigido en la cancha por Julius Randle y el entrenador en jefe Tom Thibodeau. Ambos fanáticos creen que terminarán la temporada con un premio individual. El 69 por ciento de los fanáticos de la liga creen que Randle debería ganar el premio al Jugador Más Mejorado 2020-21. El siguiente jugador más cercano, Michael Porter Jr., recibió el 11 por ciento de los votos.

                </p>
                         <br>
                         <img srcset="https://cdn.vox-cdn.com/thumbor/yjn7wwPB4MHgVtdZNTHCBTnr2GU=/0x0:1080x1350/320x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg 320w, https://cdn.vox-cdn.com/thumbor/xPn_OXkx8CdF1FGLIih_A7A0jnQ=/0x0:1080x1350/520x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg 520w, https://cdn.vox-cdn.com/thumbor/tr3Xl6TgPQIscckD4B2qp9d2_30=/0x0:1080x1350/720x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg 720w, https://cdn.vox-cdn.com/thumbor/3JMQGW-gjbS_s8Hb7quxRgl_5T0=/0x0:1080x1350/920x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg 920w, https://cdn.vox-cdn.com/thumbor/hdPYvIBJK0zavQu6iTw0JrBB0bw=/0x0:1080x1350/1120x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg 1120w, https://cdn.vox-cdn.com/thumbor/fXxgzR1yBC28sTGkFJhNsyo_FdM=/0x0:1080x1350/1320x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg 1320w, https://cdn.vox-cdn.com/thumbor/Ch3cDT_uBbpwiY1JMRwmtd2BiPg=/0x0:1080x1350/1520x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg 1520w, https://cdn.vox-cdn.com/thumbor/4n1qG0ijsh_hXlbaSXz2AoUMz4s=/0x0:1080x1350/1720x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg 1720w, https://cdn.vox-cdn.com/thumbor/muImf2aMQLEx6zCcpc6aOyT_4So=/0x0:1080x1350/1920x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg 1920w" sizes="(min-width: 1221px) 846px, (min-width: 880px) calc(100vw - 334px), 100vw" alt="" data-upload-width="1080" src="https://cdn.vox-cdn.com/thumbor/aKq-FfpK3G3a2jjoWX-C7gsqohM=/0x0:1080x1350/1200x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512685/_National_1_51221.jpg">
                                  <br>
                <p>
Hay dos posibles candidatos a los Cavs: Darius Garland y Collin Sexton. Garland parecería ser el mejor candidato considerando que dio un gran salto del año uno al año actual. Pero no es probable que un jugador de segundo año gane el premio, por lo que tal vez él u otro Cavalier podrían ganarlo en el futuro.

                </p>
                         <br>
                                  <br>
                <p>
El 50 por ciento de los fanáticos nacionales cree que Thibodeau se ha ganado el premio al Entrenador del Año esta temporada. El entrenador de los Phoenix Suns , Monty Williams, se llevó el 23 por ciento. No es de extrañar que JB Bickerstaff no esté realmente entusiasmado aquí. Sin embargo, Thibs se lo merece.


                </p>
                         <br>
                         <img srcset="https://cdn.vox-cdn.com/thumbor/rlDPnhVCl8H4h2nei1B4LDFU9kI=/0x0:1080x1350/320x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg 320w, https://cdn.vox-cdn.com/thumbor/RW7OwM_44cSJXX6TucBWH0FilWk=/0x0:1080x1350/520x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg 520w, https://cdn.vox-cdn.com/thumbor/HxB4tii9n5S_VM7xAMEYlMuK6Bo=/0x0:1080x1350/720x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg 720w, https://cdn.vox-cdn.com/thumbor/kFmgT7f7dHV0QUELwovdWIAYcaQ=/0x0:1080x1350/920x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg 920w, https://cdn.vox-cdn.com/thumbor/q_JH8psq8Va7OCjnRZvQQudlzbs=/0x0:1080x1350/1120x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg 1120w, https://cdn.vox-cdn.com/thumbor/7mMBvis02_dcaPFecoctzj5Nsno=/0x0:1080x1350/1320x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg 1320w, https://cdn.vox-cdn.com/thumbor/psYq3UAiyPbtSjlYVfYPzx9-6pg=/0x0:1080x1350/1520x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg 1520w, https://cdn.vox-cdn.com/thumbor/bGkICoJAVWnmNVpWbUy6oFxAxzM=/0x0:1080x1350/1720x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg 1720w, https://cdn.vox-cdn.com/thumbor/kMharSv2ToxoSfj8ZfRujibRKSU=/0x0:1080x1350/1920x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg 1920w" sizes="(min-width: 1221px) 846px, (min-width: 880px) calc(100vw - 334px), 100vw" alt="" data-upload-width="1080" src="https://cdn.vox-cdn.com/thumbor/A3nwsVMQKGbAmy_91113v5wCH9I=/0x0:1080x1350/1200x0/filters:focal(0x0:1080x1350):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/22512688/_National_2_51221.jpg">
                                  <br>
                <p>
Para votar en las encuestas de Reacts y que su voz sea escuchada cada semana, regístrese aquí .

                </p>
             

            </article>

    </div>


</body>



</html>