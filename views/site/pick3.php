
<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

//Deberías cambiar el título
?>

<!DOCTYPE html>
<html lang="es">
    <head>

        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>

    <div class="container">
        <body>
            <article>

                <header>

                    <h1>CAVS ELEGIRÁ EN TERCER LUGAR EN EL NBA DRAFT2021 
</h1>

                    <p class="publicacion">Publicado <time pubdate datetime="2014-03-28T20:00-04:00">2 meses atrás</time></p>

                    <img src="../../web/img/usman_1.jpg" alt="" style="
    width: 50%;
"/>
                </header>

                <p>            
                    Los Cleveland Cavaliers hicieron rebotar las bolas de lotería a su favor el martes por la noche.

                </p>
                <br>

                <p>           
                    Al llegar con la quinta mejor probabilidad para obtener la primera selección general, los Cavs se quedaron con la tercera selección general en el próximo Draft de la NBA de 2021 . Cleveland tenía un 11,2 por ciento de posibilidades de conseguir la tercera selección.

                </p>  
                <br>
                <br>
                <p>
                    Hay una serie de nombres interesantes para ver con esta selección. Uno sería Jalen Green, quien jugó con G League Ignite el año pasado y es un ala anotadora de 6'6 ”que podría encajar muy bien como un golpe anotador en el ala. Tal vez vayan con Jalen Suggs, el producto de Gonzaga que podría ser una opción interesante para crear juegos incluso si el ajuste no es limpio. Quizás Evan Mobley, el pívot de la USC que muchos consideran el talento general número 2 detrás de Cade Cunningham, se deslice por una razón u otra.

                </p>
                <br>
                <br>
                <p>
                    Independientemente de quién sea la elección y qué hagan los Cavs con ella, esta es una gran noche. No es llegar al No. 1, pero entrar en el puesto No. 3 es un buen lugar para estar. El martes es un buen día para los Cavs como organización.

                </p>


           

            </article>

    </div>


</body>



</html>