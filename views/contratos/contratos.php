<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
 use kartik\export\ExportMenu;

$js = <<< JS

 krajeeDialog.confirm = function (message, callback) {
    swal({

title: message,

type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Continuar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true,
        title: message,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, callback);
}
JS;
$this->registerJs($js, yii\web\view::POS_READY);

$this->title = 'Contratos';
$this->params['breadcrumbs'][] = ['label' => 'entrenadores', 'url' => ['consulta2']];

?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Contrato', ['create'], ['class' => 'btn']) ?>
    </p>
 

<?=

ExportMenu::widget([
    'dataProvider' => $resultados,
    'columns' => [
   [
                           'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
        'salario',
        [
        'attribute' => 'clausula_antitraspaso',
        'label' => 'cláusula antitraspaso ',
        'value' => function ($model) {
           return $model->clausula_antitraspaso;
       }
     ],
             [
        'attribute' => 'opcion_jugador',
        'label' => 'opción de jugador',
        'value' => function ($model) {
           return $model->clausula_antitraspaso;
       }
     ],
        'fecha_inicio',
        'fecha_fin',
              ['class' => 'yii\grid\ActionColumn'],
                
],
             

     
]);
    echo GridView::widget([
    'dataProvider' => $resultados,
    'columns' => [
   [
                           'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
        'salario',
        [
        'attribute' => 'clausula_antitraspaso',
        'label' => 'cláusula antitraspaso ',
        'value' => function ($model) {
           return $model->clausula_antitraspaso;
       }
     ],
             [
        'attribute' => 'opcion_jugador',
        'label' => 'opción de jugador',
        'value' => function ($model) {
           return $model->clausula_antitraspaso;
       }
     ],
        'fecha_inicio',
        'fecha_fin',
              ['class' => 'yii\grid\ActionColumn'],
                
],
             

     
]);

?>
</div>

<script>
    
//    $( "tbody tr" ).each(function( index ) {
//  console.log( index + ": " + $( this ).text() );
//  if (index%2===0) {
//      $(this).css("background-color","#5f022a").css("color","white");
//      
//    
//}
//});

//
//$('tbody td:first-child').each(function (index){
//   var enlace=$(this);
//    $(enlace).html('<a href= ' + 'view?id='+$(enlace).text().charAt(0) + '>' +$(enlace).text() + '</a>');
//    
//
//    
//});
//var i=0;
//$('tbody td:last-child a:nth-child(2)').each(function (index){
//    i++;
//   var enlace2=$(this);
//   enlace2.attr("href",'view?id='+i);
//
//});
// 


 
</script>

