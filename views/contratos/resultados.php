<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use kartik\export\ExportMenu;
$js = <<< JS

 krajeeDialog.confirm = function (message, callback) {
    swal({

title: message,

type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Continuar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true,
        title: message,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, callback);
}
JS;
$this->registerJs($js, yii\web\view::POS_READY);
$this->title = 'CONTRATOS';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
         <?php
        if (Yii::$app->user->identity->admin) {
            ?>
        <h1 id="titulo"> <?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                [
                    'label' => 'AÑO1',
                    'attribute' => 'año1',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año1);
                    }
                ],
                [
                    'label' => 'AÑO2',
                    'attribute' => 'año2',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año2);
                    }
                ],
                [
                    'label' => 'AÑO3',
                    'attribute' => 'año3',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año3);
                    }
                ],
                [
                    'label' => 'cláusula antitraspaso',
                    'attribute' => 'clausula_antitraspaso',
                    'value' => function ($model) {
                        if ($model->clausula_antitraspaso == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'label' => 'OPCIóN DE JUGADOR',
                    'attribute' => 'opcion_jugador',
                    'value' => function ($model) {
                        if ($model->opcion_jugador == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'label' => 'fecha de inicio ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_inicio));
                    }
                ],
                [
                    'attribute' => 'fecha_fin',
                    'label' => 'fecha de fin ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_fin));
                    }
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update}{delete}',
                ],
            ]
        ]);?><?= Html::encode($this->title)?> <?= Html::a('Añadir Contrato', ['create_1'], ['class' => 'btn']) ?></h1>



        <?=
      GridView::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                [
                    'label' => 'AÑO1',
                    'attribute' => 'año1',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año1);
                    }
                ],
                [
                    'label' => 'AÑO2',
                    'attribute' => 'año2',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año2);
                    }
                ],
                [
                    'label' => 'AÑO3',
                    'attribute' => 'año3',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año3);
                    }
                ],
                [
                    'label' => 'cláusula antitraspaso',
                    'attribute' => 'clausula_antitraspaso',
                    'value' => function ($model) {
                        if ($model->clausula_antitraspaso == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'label' => 'OPCIóN DE JUGADOR',
                    'attribute' => 'opcion_jugador',
                    'value' => function ($model) {
                        if ($model->opcion_jugador == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'label' => 'fecha de inicio ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_inicio));
                    }
                ],
                [
                    'attribute' => 'fecha_fin',
                    'label' => 'fecha de fin ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_fin));
                    }
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update}{delete}',
                ],
            ]
        ]);
        ?>
         <?php
        } else {
            ?>
<h1 id="titulonoadmin"> <?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                [
                    'label' => 'AÑO1',
                    'attribute' => 'año1',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año1);
                    }
                ],
                [
                    'label' => 'AÑO2',
                    'attribute' => 'año2',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año2);
                    }
                ],
                [
                    'label' => 'AÑO3',
                    'attribute' => 'año3',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año3);
                    }
                ],
                [
                    'label' => 'clausula antitraspaso',
                    'attribute' => 'clausula_antitraspaso',
                    'value' => function ($model) {
                        if ($model->clausula_antitraspaso == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'label' => 'OPCION DE JUGADOR',
                    'attribute' => 'opcion_jugador',
                    'value' => function ($model) {
                        if ($model->opcion_jugador == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'label' => 'fecha de inicio ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_inicio));
                    }
                ],
                [
                    'attribute' => 'fecha_fin',
                    'label' => 'fecha de fin ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_fin));
                    }
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update}{delete}',
                ],
            ]
        ]);?><?= Html::encode($this->title)?></h1>



        <?=
      GridView::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                [
                    'label' => 'AÑO1',
                    'attribute' => 'año1',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año1);
                    }
                ],
                [
                    'label' => 'AÑO2',
                    'attribute' => 'año2',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año2);
                    }
                ],
                [
                    'label' => 'AÑO3',
                    'attribute' => 'año3',
                    'value' => function ($model) {
                        return ' $ '.number_format($model->año3);
                    }
                ],
                [
                    'label' => 'clausula antitraspaso',
                    'attribute' => 'clausula_antitraspaso',
                    'value' => function ($model) {
                        if ($model->clausula_antitraspaso == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'label' => 'OPCION DE JUGADOR',
                    'attribute' => 'opcion_jugador',
                    'value' => function ($model) {
                        if ($model->opcion_jugador == 0) {
                            return 'NO';
                        } else {
                            return 'SI';
                        }
                    }
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'label' => 'fecha de inicio ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_inicio));
                    }
                ],
                [
                    'attribute' => 'fecha_fin',
                    'label' => 'fecha de fin ',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_fin));
                    }
                ],
               
            ]
        ]);
                 }
        ?>
        
    </div>
</div>

<script>
    $('table td:first-child').css('text-align', 'initial')

</script>

