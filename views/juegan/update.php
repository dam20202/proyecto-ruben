<?php

use kartik\form\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Jugadores;
use app\models\Entrenamientos;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container">
<div class="juegan-form">
  <h1 id="tituloform">AÑADIR ESTADISTICAS</h1>
  
<?php
$codigo=$model->getOldAttribute('cod_jugador');
  $jugadores = Jugadores::find()
        ->select('nombre,apellidos')
        ->Where("cod_jugador=" . $codigo)
        ->one();
$array = [
    ['id' => $model->cod_jugador, 'data' =>$model->nombre ],
   
];

   $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
//   esto es para coger el codigo del jugador, si se borra el cod es null
  

   echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'cod_jugador' => ['label' => 'Jugador', 'type' => Form::INPUT_TEXT, 'options' => ['readonly'=>true,'placeholder'=>$jugadores->nombre.' '.$jugadores->apellidos]],
                'cod_partido' => ['label' => 'Partido', 'type' => Form::INPUT_TEXT, 'options' => ['readonly'=>true]],
               
            ]
        ]);
    echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => [
                'puntos_jugador' => ['label' => 'Puntos', 'type' => Form::INPUT_TEXT,],
                  'rebotes_jugador' => ['label' => 'Rebotes', 'type' => Form::INPUT_TEXT,],
                  'asistencias_jugador' => ['label' => 'Asistencias', 'type' => Form::INPUT_TEXT,],
               
            ]
        ]);
       echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => [
                'minutos_jugador' => ['label' => 'Minutos jugados', 'type' => Form::INPUT_TEXT,],
                  'robos' => ['label' => 'Robos', 'type' => Form::INPUT_TEXT,],
                  'tapones' => ['label' => 'Tapones', 'type' => Form::INPUT_TEXT,],
               
            ]
        ]);
         echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'tiros_jugador' => ['label' => 'Tiros de campo', 'type' => Form::INPUT_TEXT,],
                  'aciertos_jugador' => ['label' => 'Tiros acertados', 'type' => Form::INPUT_TEXT,],
               
            ]
        ]);
           echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                't3_intentados' => ['label' => 'Triples intentados', 'type' => Form::INPUT_TEXT,],
                  't3_acertados' => ['label' => 'Triples acertados', 'type' => Form::INPUT_TEXT,],
           
               
            ]
        ]);
             echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'tl_intentados' => ['label' => 'Tiros libres intentados', 'type' => Form::INPUT_TEXT,],
                  'tl_acertados' => ['label' => 'Tiros libres acertados', 'type' => Form::INPUT_TEXT,],
           
               
            ]
        ]);

   echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                '' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<div style="  text-align: right;margin-top: 6px;padding-top: 20px;padding-bottom: 52px;margin-right: -30px;padding-right: 30px;background-color: #EDEDED;margin-left: -30px;">' .
                    Html::resetButton('Reset', ['class' => 'btn', 'id' => 'btnf']) . ' ' .
                    Html::submitButton('Añadir', ['class' => 'btn', 'id' => 'btnf']) .
                    '</div>'
                ],
            ]
        ]);

    

    ActiveForm::end(); ?>

</div>
    </div>
