<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\helpers\Url;
use aryelds\sweetalert\SweetAlert;
use kartik\export\ExportMenu;

$this->title = 'PARTIDOS';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
         
          <?= Html::a('Estadísticas', ['vistaindividual','cod'=>$cod], ['class' => 'btn', 'id' => 'boton1']) ?>
        <?= Html::a('Repetición', ['partido','cod'=>$cod], ['class' => 'btn', 'id' => 'boton2']) ?>

 <?php
        if (Yii::$app->user->identity->admin) {
            ?>
        <h1 id="titulo" style=margin-top:51px;>  <?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            
            'columns' => [
               [
                           'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                'puntos_jugador',
                'rebotes_jugador',
                'asistencias_jugador',
                'robos',

              
                [
                    'label' => 'TC',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
               'minutos',
            ],
        ]);?>
          Estadisticas individuales<?= Html::a('Editar partido', ['partidos/update','id'=>$cod], ['class' => 'btn']) ?></h1>
      



        <?=
      
 GridView::widget([
            'dataProvider' => $resultados,
            
            'columns' => [
               [
                           'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                'puntos_jugador',
                'rebotes_jugador',
                'asistencias_jugador',
                'robos',

              
                [
                    'label' => 'TC',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
               'minutos',
            ],
        ]);
          

?>
         <?php
        } else {
            ?>
        <h1 id="titulonoadmin" style=margin-top:51px;> <?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            
            'columns' => [
               [
                           'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                'puntos_jugador',
                'rebotes_jugador',
                'asistencias_jugador',
                'robos',

              
                [
                    'label' => 'TC',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
               'minutos',
            ],
        ]);?>
          Estadisticas individuales</h1>
      



        <?=
      
 GridView::widget([
            'dataProvider' => $resultados,
            
            'columns' => [
               [
                           'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '' . $model->nombre . ' ' . $model->apellidos;
                    },
                ],
                'puntos_jugador',
                'rebotes_jugador',
                'asistencias_jugador',
                'robos',

              
                [
                    'label' => 'TC',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
               'minutos',
            ],
        ]);
         }

?>
 
 
  

    </div>

</div>


<script>

    $('table td:first-child').css('text-align', 'initial')
</script>

