<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partidos-index">
    <div class="container">
    <h1><?= Html::encode($this->title) ?></h1>

   


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
          
            'puntos_rivales',
            'nombre_rival',
            'imagen',
            'estadio',
            //'mes',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
     <p>
        <?= Html::a('Create Partidos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    </div>



</div>
