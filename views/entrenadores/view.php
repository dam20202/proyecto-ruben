<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenadores */

$this->title = $model->cod_entrenador;
$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container">
<div class="entrenadores-view">
   

    <h1 id="titulopartidos"><?= Html::encode($model->nombre.' '.$model->apellidos) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_entrenador',
            'nombre',
            'apellidos',
            'cargo',
            'num_victorias',
            'num_derrotas',
            'imagen',
        ],
    ]) ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->cod_entrenador], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->cod_entrenador], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
    </div>
