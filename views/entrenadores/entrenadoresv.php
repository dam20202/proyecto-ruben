<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;

$js = <<< JS

 krajeeDialog.confirm = function (message, callback) {
    swal({

title: message,

type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Continuar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true,
        title: message,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, callback);
}
JS;
$this->registerJs($js, yii\web\view::POS_READY);
$this->title = 'Entrenadores';
$this->params['breadcrumbs'][] = ['label' => 'entrenadores', 'url' => ['consulta2']];
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
        <?php
        if (Yii::$app->user->identity->admin) {
            ?>
            <h1 id="titulo"><?=
                ExportMenu::widget([
                    'dataProvider' => $resultados,
                    'columns' => [
                        [
                            'attribute' => 'nombre',
                            'value' => function ($data) {
                                return Html::a($data->nombre . " " . $data->apellidos, Url::to(['entrenadores/view?id=' . $data->cod_entrenador]));
                            },
                            'format' => 'raw',
                        ],
                        'cargo',
                                    'telefono',
                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{update}{delete}',]
                    ],
                ]);
                ?><?= Html::encode($this->title) ?><?= Html::a('Añadir Entrenador', ['create'], ['class' => 'btn']) ?></h1>



            <?=
            GridView::widget([
                'dataProvider' => $resultados,
                'columns' => [
                    [
                        'attribute' => 'nombre',
                        'value' => function ($data) {
                            return Html::a($data->nombre . " " . $data->apellidos, Url::to(['entrenadores/view?id=' . $data->cod_entrenador]));
                        },
                        'format' => 'raw',
                    ],
                    'cargo',
                     ['attribute' => 'telefono',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a($model->telefono, "tel:" . $model->telefono);
                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{update}{delete}',]
                ],
            ]);
            ?>
            <?php
        } else {
            ?>
            <h1 id="titulonoadmin"><?=
                ExportMenu::widget([
                    'dataProvider' => $resultados,
                    'columns' => [
                        [
                            'attribute' => 'nombre',
                            'value' => function ($data) {
                                return Html::a($data->nombre . " " . $data->apellidos, Url::to(['entrenadores/view?id=' . $data->cod_entrenador]));
                            },
                            'format' => 'raw',
                        ],
                        'cargo',
                                       'telefono',
                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{update}{delete}',]
                    ],
                ]);
                ?><?= Html::encode($this->title) ?></h1>



            <?=
            GridView::widget([
                'dataProvider' => $resultados,
                'columns' => [
                    [
                        'attribute' => 'nombre',
                        'value' => function ($data) {
                            return Html::a($data->nombre . " " . $data->apellidos, Url::to(['entrenadores/view?id=' . $data->cod_entrenador]));
                        },
                        'format' => 'raw',
                    ],
                    'cargo',
                    ['attribute' => 'telefono',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a($model->telefono, "tel:" . $model->telefono);
                        }
                    ],
                ],
            ]);
        }
        ?>


    </div>
</div>

