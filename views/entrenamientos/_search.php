<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EntrenamientosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entrenamientos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cod_entrenamiento') ?>

    <?= $form->field($model, 'duracion') ?>

    <?= $form->field($model, 'cod_entrenador') ?>

    <?= $form->field($model, 'Tipo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
