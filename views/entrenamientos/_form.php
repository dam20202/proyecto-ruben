<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenamientos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">
    <div class="container">
 
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'duracion')->textInput() ?>

    <?= $form->field($model, 'cod_entrenador')->textInput() ?>

    <?= $form->field($model, 'Tipo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>