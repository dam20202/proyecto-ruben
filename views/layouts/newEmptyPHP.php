 <footer class="footer" function="footer">
            <div id="divf1"class="container">
                
                
                <h4 class="pane-title">ADDITIONAL INFO</h4>
                <p>
                    <a href="https://www.rocketmortgagefieldhouse.com/guest-experience/accessibility" title="Accessibility" style="color: white">Accessibility</a><br>
                    <a href="https://www.rocketmortgagefieldhouse.com/plan-your-visit/directions-parking" title="Directions to Rocket Mortgage FieldHouse" style="color: white">Directions</a><br>
                    <a href="https://www.nba.com/cavaliers/contact" title="Contact Us" style="color: white">Have a Great Idea?</a><br>
                    <a href="https://www.teamworkonline.com/basketball-jobs/cleveland-cavaliers/cleveland-cavaliers-jobs" title="Job Opportunities" style="color: white">Job Opportunities</a><br>
                    <a href="https://www.rocketmortgagefieldhouse.com/" title="Rocket Mortgage FieldHouse" style="color: white"Rocket Mortgage FieldHouse</a><br>
                </p>
                <br>


                <p class="copyrights">
                    Copyright © 2021 NBA Media Ventures, LLC. All rights reserved.<br>
                    No portion of NBA.com may be duplicated, redistributed or <br>
                    manipulated in any form.This site is operated jointly by NBA<br> 
                    and WarnerMedia. To optout of the sale of your personal <br>
                    information as permitted by the California Consumer Privacy<br>
                    Act, please use the links below to visit each company’s <br>
                    privacy center. If you make a request through the WarnerMedia<br> 
                    Privacy Center,it will apply to data controlled jointly by the<br> 
                    NBA and WarnerMedia as well as other data controlled by <br>
                    WarnerMedia. If you make a request through the NBA Privacy Center,<br>
                    it will apply to data controlled independently by the NBA.<br>
                    <a href="https://www.nba.com/privacy-policy#Cookies" style="color: white">NBA Privacy Center</a>  |
                    <a href="https://www.warnermediaprivacy.com"style="color: white">WarnerMedia Privacy Center</a>  |<br>
                    <a href="https://privacy.nba.com"style="color: white">Do Not Sell My Personal Information</a>  |
                    <a href="http://www.nba.com/news/termsofuse"style="color: white">Terms of Use</a>  |
                    <a href="https://www.nba.com/accessibility"style="color: white">Accessibility and <br>Closed Caption</a>  |


                </p>
                <?= Html::img('@web/img/rocket.png', ['id' => 'rocket']); ?>
            </div>



        </footer>