<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Involucrans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="involucran-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Involucran', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_involucran',
            'cod_jugador',
            'cod_traspaso',
            'equipo_inicial',
            'equipo_final',
            //'imagen',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
