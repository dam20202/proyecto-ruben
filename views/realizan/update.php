<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Realizan */

$this->title = 'Update Realizan: ' . $model->cod_realizan;
$this->params['breadcrumbs'][] = ['label' => 'Realizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_realizan, 'url' => ['view', 'id' => $model->cod_realizan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="realizan-update">

    <h1></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
