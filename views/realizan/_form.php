<?php

use kartik\form\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Jugadores;
use kartik\datecontrol\DateControl;
use app\models\Entrenamientos;

/* @var $this yii\web\View */
/* @var $model app\models\Realizan */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="../js/jquery.js"></script>
<div class="form">
    <div class="container">
        <h1 id="tituloform">ENTRENAMIENTOS</h1>

        <?php
        $entrenos = ArrayHelper::map(Entrenamientos::find()->all()
                        , 'cod_entrenamiento', 'Tipo');

        $jugadores = Jugadores::find()
                ->where('agente_libre=0')
                ->all();
    

        foreach ($jugadores as &$jugador) {
            $jugador->nombre = $jugador->nombre . ' ' . $jugador->apellidos;
        }

        $items = ArrayHelper::map($jugadores, 'cod_jugador', 'nombre');
// Todo With activities
//   
//   var_dump($items);
        $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => [
                'cod_jugador' => ['label' => 'Jugador', 'type' => Form::INPUT_DROPDOWN_LIST, 'items' =>
                    $items,
                    ],
                'cod_entrenamiento' => ['label' => 'Entrenamiento', 'type' => Form::INPUT_DROPDOWN_LIST, 'items' =>
                    $entrenos,
                   ],
            ]
        ]);
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'calorias_jugador' => ['label' => 'Calorías', 'type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Calorías quemadas por el jugador']],
                'distancia_jugador' => ['label' => 'Distancia Recorrida', 'type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Distancia recoorida por el jugador']],
//        'puesto'=>['type'=>Form::INPUT_DROPDOWN_LIST, 'items'=>['BA' => 'BA', 'ES' => 'ES', 'AL' => 'AL','AP'=>'AP','P'=>'P'], 'options'=>['placeholder'=>'Enter password...']],
            ]
        ]);

       echo $form->field($model, 'fecha')->widget(DateControl::classname(), [
    'type' => 'date',
    'ajaxConversion' => true,
    'autoWidget' => true,
    'widgetClass' => '',
    'displayFormat' => 'dd-MM-yyyy',
    'saveFormat' => 'php:Y-m-d',
    'saveTimezone' => 'Europe/Brussels',
    'displayTimezone' => 'Europe/Brussels',
   
     'widgetOptions' => [
        'pluginOptions' => [
            'endDate'=> date('d-m-Y'),
            'startDate'=> date('d-m-Y',strtotime('-12 months')),
            'autoclose' => true,
            
        ]
    ],
    'language' => 'es'
]);

        echo '<br>';
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                '' => [
                    'type' => Form::INPUT_RAW,
                    'value' => '<div style="  text-align: right;margin-top: 6px;padding-top: 20px;padding-bottom: 52px;margin-right: -30px;padding-right: 30px;background-color: #EDEDED;margin-left: -30px;">' .
                     Html::resetButton('Borrar', ['class' => 'btn', 'id' => 'btnf']) . ' ' .
                    Html::submitButton('Confirmar', ['class' => 'btn', 'id' => 'btnf']) .
                    '</div>'
                ],
            ]
        ]);


//echo Html::button('Submit', ['type'=>'button', 'class'=>'btn btn-primary']);
        ActiveForm::end();
        ?>

    </div>
</div>
<script>




</script>


