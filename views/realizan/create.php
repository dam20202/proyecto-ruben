<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Realizan */

$this->title = 'Create Realizan';
$this->params['breadcrumbs'][] = ['label' => 'Realizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realizan-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
