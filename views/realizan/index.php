<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RealizanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Realizans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realizan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Realizan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_realizan',
            'cod_jugador',
            'cod_entrenamiento',
            'distancia_jugador',
            'calorias_jugador',
            //'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
