<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */

$this->title = 'Create Juegan';
$this->params['breadcrumbs'][] = ['label' => 'Juegans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

   
?>
<script src="../js/jquery.js"></script>

 <?php
$numpartidos = app\models\Partidos::find()
                ->select('count(*)')
                ->scalar();
 
 
   ?>
<div id="cartas" class="col-lg-6">

          <div id="subcartas3" class="col-lg-1" style="margin-top:10px;">
              <h1 style="text-align: center;">  <?php
         echo  $model['numero'];
          ?></h1>
              <h1 style="text-align: center;">
                 <?php
         echo '<p>' . $model['puesto'].'</p>';
          ?></h1>
                 </div>
    
    <div id="subcartas" class="col-lg-5">
      
        <?php
 
               echo Html::img('@web/img/'.$model['nombre'].'.png');
        ?>
    </div>
    <div id="subcartas2" class="col-lg-6"style="display: flex;flex-direction: column;flex-wrap: wrap;align-items: flex-end;">
        <?php
      
 

        echo '<h1>' . $model['nombre'].' '.$model['apellidos'].'</h1>';
     
        ?>
    
   <ul id="info">
            <li>
              <p>EDAD
                <span class="xs-hide">&nbsp;</span>
                <br>
                <span class="info-value"><?=$model['fecha'] ?></span>
              </p>
            </li>
            <li>
              <p>ALTURA
                <span class="xs-hide">&nbsp;</span>
                <br>
                <span class="info-value"><?=$model['altura'] ?></span>
              </p>
            </li>
            <li>
              <p>ANCHO
                <span class="xs-hide">&nbsp;</span>
                <br>
                <span class="info-value"><?=$model['envergadura'] ?></span>
              </p>
            </li>
          </ul>
       
        <table class="averages">
            <tbody>
                <tr class="averages-labels">
              <td>
                <p>PJ</p>
              </td>
              <td>
                <p>PPP</p>
              </td>
              <td>
                <p>RPP</p>
              </td>
              <td>
                <p>APP</p>
              </td>
            </tr>
            <tr class="averages-season">
              <td>
                <p><?=$model['partidosjugados'] ?></p>
              </td>
              <td>
                <p><?=floor($model['puntos'] * 100) / 100?></p>
              </td>
              <td>
                <p><?=floor($model['rebotes'] * 100) / 100?></p>
              </td>
              <td>
                <p><?=floor($model['asistencias'] * 100) / 100?></p>
              </td>
            </tr>
          </tbody></table>
             
     
    </div>
    

    
    </div>

<script>
    
// $("#subcartas3 > h1").each(function () {
//     var numero = parseInt($(this).text());
//      if (numero == 2) {
//
//            $(this).text('VICTORIA').css('color', 'green');
//        } else {
//            $(this).text('DERROTA').css('color', 'red');
//        }
//     });
//
//    



</script>


