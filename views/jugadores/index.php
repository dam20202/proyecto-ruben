<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jugadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Jugadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_jugador',
            'nombre',
            'apellidos',
            'lesion',
            'años_carrera',
            //'puesto',
            //'envergadura',
            //'altura',
            //'numero',
            //'imagen',
            //'puntos_partido',
            //'rebotes_partido',
            //'asistencias_partido',
            //'agente_libre',
            //'estado',
            //'equipo_anterior',
            //'liga',
            //'años',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
