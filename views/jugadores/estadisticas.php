<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;

$this->title = 'JUGADORES';
$this->params['breadcrumbs'][] = $this->title;
?>

<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">

        <h1 id="titulouser"><?= Html::encode($this->title) ?></h1>




        <?=
        GridView::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                  [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '<p>' . $model->nombre . ' ' . $model->apellidos . '</p>';
                    },
                ],
                'puesto',
                'altura',
                'envergadura',
                [
                    'label' => 'PUNTOS',
                    'attribute' => 'puntos',
                    'value' => function ($model) {
                        return floor($model->puntos * 100) / 100;
                    }
                ],
                [
                    'label' => 'REBOTES',
                    'attribute' => 'rebotes',
                    'value' => function ($model) {
                        return floor($model->rebotes * 100) / 100;
                    }
                ],
                [
                    'label' => 'ASISTENCIAS',
                    'attribute' => 'asistencias',
                    'value' => function ($model) {
                        return floor($model->asistencias * 100) / 100;
                    }
                ],
//             'tapones',
                [
                    'label' => 'ROBOS',
                    'attribute' => 'robos',
                    'value' => function ($model) {
                        return floor($model->rebotes * 100) / 100;
                    }
                ],
                [
                    'label' => 'TC',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
                'minutos',
                
            ],
        ]);
        ?>
       


    </div>

</div>
<script>
    $('table td:first-child').css('text-align', 'initial')

</script>

