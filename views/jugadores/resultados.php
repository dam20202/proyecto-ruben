<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use kartik\export\ExportMenu;
$js = <<< JS

 krajeeDialog.confirm = function (message, callback) {
    swal({

title: message,

type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Continuar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true,
        title: message,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, callback);
}
JS;
$this->registerJs($js, yii\web\view::POS_READY);

$this->title = 'JUGADORES';
$this->params['breadcrumbs'][] = $this->title;
?>

<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
        <?php
        if (Yii::$app->user->identity->admin) {
            ?>
            <h1 id="titulo"> <?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            'columns' => [
                [
                    'attribute' => 'nombre',
                    'format' => 'html',
                    'label' => 'JUGADOR',
                    'value' => function ($model) {

                        return Html::img('../../web/img/' . $model->nombre . '.png',
                                        ['width' => '60px']) . '<p>' . $model->nombre . ' ' . $model->apellidos . '</p>';
                    },
                ],
                [
                    'label' => 'Fecha de nacimiento',
                    'attribute' => 'fecha_nacimiento',
                    'value' => function ($model) {
                        return date("d-m-Y", strtotime($model->fecha_nacimiento));
                    }
                ],
                'puesto',
                'altura',
                'envergadura',
                [
                    'label' => 'PUNTOS',
                    'attribute' => 'puntos',
                    'value' => function ($model) {
                        return floor($model->puntos * 100) / 100;
                    }
                ],
                [
                    'label' => 'REBOTES',
                    'attribute' => 'rebotes',
                    'value' => function ($model) {
                        return floor($model->rebotes * 100) / 100;
                    }
                ],
                [
                    'label' => 'ASISTENCIAS',
                    'attribute' => 'asistencias',
                    'value' => function ($model) {
                        return floor($model->asistencias * 100) / 100;
                    }
                ],
//             'tapones',
                [
                    'label' => 'ROBOS',
                    'attribute' => 'robos',
                    'value' => function ($model) {
                        return floor($model->rebotes * 100) / 100;
                    }
                ],
                [
                    'label' => 'TC',
                    'attribute' => 'TC',
                    'value' => function ($model) {
                        return floor($model->TC * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'T3',
                    'attribute' => 'T3',
                    'value' => function ($model) {
                        return floor($model->T3 * 100) / 100 . '%';
                    }
                ],
                [
                    'label' => 'TL',
                    'attribute' => 'TL',
                    'value' => function ($model) {
                        return floor($model->TL * 100) / 100 . '%';
                    }
                ],
                'minutos',
            ],
        ]);
            ?><?= Html::encode($this->title) ?> <?= Html::a('Añadir Jugador', ['create'], ['class' => 'btn']) ?></h1>



    <?=
    GridView::widget([
        'dataProvider' => $resultados,
        //usamos esto para poder concatenar las columnas.
        'columns' => [
            [
                'attribute' => 'nombre',
                'format' => 'html',
                'label' => 'JUGADOR',
                'value' => function ($model) {

                    return Html::img('../../web/img/' . $model->nombre . '.png',
                                    ['width' => '60px']) . '<p>' . $model->nombre . ' ' . $model->apellidos . '</p>';
                },
            ],
            [
                'label' => 'Fecha de nacimiento',
                'attribute' => 'fecha_nacimiento',
                'value' => function ($model) {
                    return date("d-m-Y", strtotime($model->fecha_nacimiento));
                }
            ],
            'puesto',
            'altura',
            'envergadura',
            [
                'label' => 'PUNTOS',
                'attribute' => 'puntos',
                'value' => function ($model) {
                    return floor($model->puntos * 100) / 100;
                }
            ],
            [
                'label' => 'REBOTES',
                'attribute' => 'rebotes',
                'value' => function ($model) {
                    return floor($model->rebotes * 100) / 100;
                }
            ],
            [
                'label' => 'ASISTENCIAS',
                'attribute' => 'asistencias',
                'value' => function ($model) {
                    return floor($model->asistencias * 100) / 100;
                }
            ],
//             'tapones',
            [
                'label' => 'ROBOS',
                'attribute' => 'robos',
                'value' => function ($model) {
                    return floor($model->rebotes * 100) / 100;
                }
            ],
            [
                'label' => 'TC',
                'attribute' => 'TC',
                'value' => function ($model) {
                    return floor($model->TC * 100) / 100 . '%';
                }
            ],
            [
                'label' => 'T3',
                'attribute' => 'T3',
                'value' => function ($model) {
                    return floor($model->T3 * 100) / 100 . '%';
                }
            ],
            [
                'label' => 'TL',
                'attribute' => 'TL',
                'value' => function ($model) {
                    return floor($model->TL * 100) / 100 . '%';
                }
            ],
            'minutos',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]);
    ?>

            <?php
        } else {
            ?>
            <h1 id="titulonoadmin"> <?=
            ExportMenu::widget([
                'dataProvider' => $resultados,
                'columns' => [
                    [
                        'attribute' => 'nombre',
                        'format' => 'html',
                        'label' => 'JUGADOR',
                        'value' => function ($model) {

                            return Html::img('../../web/img/' . $model->nombre . '.png',
                                            ['width' => '60px']) . '<p>' . $model->nombre . ' ' . $model->apellidos . '</p>';
                        },
                    ],
                    [
                        'label' => 'Fecha de nacimiento',
                        'attribute' => 'fecha_nacimiento',
                        'value' => function ($model) {
                            return date("d-m-Y", strtotime($model->fecha_nacimiento));
                        }
                    ],
                    'puesto',
                    'altura',
                    'envergadura',
                    [
                        'label' => 'PUNTOS',
                        'attribute' => 'puntos',
                        'value' => function ($model) {
                            return floor($model->puntos * 100) / 100;
                        }
                    ],
                    [
                        'label' => 'REBOTES',
                        'attribute' => 'rebotes',
                        'value' => function ($model) {
                            return floor($model->rebotes * 100) / 100;
                        }
                    ],
                    [
                        'label' => 'ASISTENCIAS',
                        'attribute' => 'asistencias',
                        'value' => function ($model) {
                            return floor($model->asistencias * 100) / 100;
                        }
                    ],
//             'tapones',
                    [
                        'label' => 'ROBOS',
                        'attribute' => 'robos',
                        'value' => function ($model) {
                            return floor($model->rebotes * 100) / 100;
                        }
                    ],
                    [
                        'label' => 'TC',
                        'attribute' => 'TC',
                        'value' => function ($model) {
                            return floor($model->TC * 100) / 100 . '%';
                        }
                    ],
                    [
                        'label' => 'T3',
                        'attribute' => 'T3',
                        'value' => function ($model) {
                            return floor($model->T3 * 100) / 100 . '%';
                        }
                    ],
                    [
                        'label' => 'TL',
                        'attribute' => 'TL',
                        'value' => function ($model) {
                            return floor($model->TL * 100) / 100 . '%';
                        }
                    ],
                    'minutos',
                ],
            ]);
            ?><?= Html::encode($this->title) ?></h1>



                <?=
                GridView::widget([
                    'dataProvider' => $resultados,
                    //usamos esto para poder concatenar las columnas.
                    'columns' => [
                        [
                            'attribute' => 'nombre',
                            'format' => 'html',
                            'label' => 'JUGADOR',
                            'value' => function ($model) {

                                return Html::img('../../web/img/' . $model->nombre . '.png',
                                                ['width' => '60px']) . '<p>' . $model->nombre . ' ' . $model->apellidos . '</p>';
                            },
                        ],
                        [
                            'label' => 'Fecha de nacimiento',
                            'attribute' => 'fecha_nacimiento',
                            'value' => function ($model) {
                                return date("d-m-Y", strtotime($model->fecha_nacimiento));
                            }
                        ],
                        'puesto',
                        'altura',
                        'envergadura',
                        [
                            'label' => 'PUNTOS',
                            'attribute' => 'puntos',
                            'value' => function ($model) {
                                return floor($model->puntos * 100) / 100;
                            }
                        ],
                        [
                            'label' => 'REBOTES',
                            'attribute' => 'rebotes',
                            'value' => function ($model) {
                                return floor($model->rebotes * 100) / 100;
                            }
                        ],
                        [
                            'label' => 'ASISTENCIAS',
                            'attribute' => 'asistencias',
                            'value' => function ($model) {
                                return floor($model->asistencias * 100) / 100;
                            }
                        ],
//             'tapones',
                        [
                            'label' => 'ROBOS',
                            'attribute' => 'robos',
                            'value' => function ($model) {
                                return floor($model->rebotes * 100) / 100;
                            }
                        ],
                        [
                            'label' => 'TC',
                            'attribute' => 'TC',
                            'value' => function ($model) {
                                return floor($model->TC * 100) / 100 . '%';
                            }
                        ],
                        [
                            'label' => 'T3',
                            'attribute' => 'T3',
                            'value' => function ($model) {
                                return floor($model->T3 * 100) / 100 . '%';
                            }
                        ],
                        [
                            'label' => 'TL',
                            'attribute' => 'TL',
                            'value' => function ($model) {
                                return floor($model->TL * 100) / 100 . '%';
                            }
                        ],
                        'minutos',
                    ],
                ]);
            }
            ?>



    </div>

</div>
<script>
    $('table td:first-child').css('text-align', 'initial')

</script>

