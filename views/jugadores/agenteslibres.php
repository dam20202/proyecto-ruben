<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\helpers\Url;
use kartik\export\ExportMenu;
$js = <<< JS

 krajeeDialog.confirm = function (message, callback) {
    swal({

title: message,

type: "warning",
showCancelButton: true,
confirmButtonColor: "#5f022a",
confirmButtonText: "Continuar",
cancelButtonText: "Cancelar",
closeOnConfirm: false,
closeOnCancel: true,
        title: message,
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
        allowOutsideClick: true
    }, callback);
}
JS;
$this->registerJs($js, yii\web\view::POS_READY);
$this->title = 'AGENTES LIBRES';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="../js/jquery.js"></script>
<div class="jugadores-index">
    <div class="container">
        <?php
        if (Yii::$app->user->identity->admin) {
            ?>
        <h1 id="titulo"><?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                'nombre',
                'años_carrera',
                'puesto',
                'puntos_partido',
                'rebotes_partido',
                'asistencias_partido',
                'equipo_anterior',
                'liga',
                'años',
//        'agente_libre',
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{fichar}{delete}',
                    'buttons' => [
                        'fichar' => function ($url, $model) {

                            return Html::a('<span class="glyphicon glyphicon-briefcase"</span>', [
                                        'jugadores/crearagente', 'id' => $model->cod_jugador
                            ]);
                        }
//                              ,
//                              'gg'=> function ($url,$model) {	
//                                    $url = Url::to(['jugadores/deleteagente', 'id' => $model->cod_jugador]);
//
//                                return Html::a('<span class="glyphicon glyphicon-trash"</span>',$url); 
//                              }
                    ]
                ],
            ]
        ]);
        ?><?= Html::encode($this->title) ?><?= Html::a('Añadir Agente Libre', ['createagente'], ['class' => 'btn']) ?></h1>

      


        <input type="text" id="buscador" class="form-control" placeholder="Buscar...">
        <?=
        GridView::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                'nombre',
                'años_carrera',
                'puesto',
                'puntos_partido',
                'rebotes_partido',
                'asistencias_partido',
                'equipo_anterior',
                'liga',
                 [
                     'attribute'=>'años',
                    'label' => 'EDAD',
                    'value' => function ($model) {
                        return $model->años;
                    }
                ],
//        'agente_libre',
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{fichar}{delete}',
                    'buttons' => [
                        'fichar' => function ($url, $model) {

                            return Html::a('<span class="glyphicon glyphicon-briefcase"</span>', [
                                        'jugadores/crearagente', 'id' => $model->cod_jugador
                            ],['title' => 'Fichar']);
                        }
//                              ,
//                              'gg'=> function ($url,$model) {	
//                                    $url = Url::to(['jugadores/deleteagente', 'id' => $model->cod_jugador]);
//
//                                return Html::a('<span class="glyphicon glyphicon-trash"</span>',$url); 
//                              }
                    ]
                ],
            ]
        ]);
        ?>
         <?php
        } else {
            ?>
        <h1 id="titulonoadmin"><?=
        ExportMenu::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                'nombre',
                'años_carrera',
                'puesto',
                'puntos_partido',
                'rebotes_partido',
                'asistencias_partido',
                'equipo_anterior',
                'liga',
                'años',
//        'agente_libre',
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{fichar}{delete}',
                    'buttons' => [
                        'fichar' => function ($url, $model) {

                            return Html::a('<span class="glyphicon glyphicon-briefcase"</span>', [
                                        'jugadores/crearagente', 'id' => $model->cod_jugador
                            ]);
                        }
//                              ,
//                              'gg'=> function ($url,$model) {	
//                                    $url = Url::to(['jugadores/deleteagente', 'id' => $model->cod_jugador]);
//
//                                return Html::a('<span class="glyphicon glyphicon-trash"</span>',$url); 
//                              }
                    ]
                ],
            ]
        ]);
        ?><?= Html::encode($this->title) ?></h1>

      


        <input type="text" id="buscador" class="form-control" placeholder="Buscar...">
        <?=
        GridView::widget([
            'dataProvider' => $resultados,
            //usamos esto para poder concatenar las columnas.
            'columns' => [
                'nombre',
                'años_carrera',
                'puesto',
                'puntos_partido',
                'rebotes_partido',
                'asistencias_partido',
                'equipo_anterior',
                'liga',
                 [
                     'attribute'=>'años',
                    'label' => 'EDAD',
                    'value' => function ($model) {
                        return $model->años;
                    }
                ],
//        'agente_libre',
               
            ]
        ]);
                 }
        ?>
    </div>

</div>
<script>
    $("#buscador").on("keyup", function () {

        var value = $(this).val().toLowerCase();
//  if(!$.isNumeric(value)){
        $(".table tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
//    }
    });

</script>

